FROM registry.gitlab.com/re_secco/question-core-eta-2022.1:dependencies

COPY . .

RUN mkdir -p assets/static \
  && python manage.py collectstatic --noinput

CMD ["python","-u", "production-server.py"]
